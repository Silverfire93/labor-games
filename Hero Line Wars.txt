Hero Line Wars 2v2

	Mobile:
		-Mobile Spieler kann dem Gegnerischen Team Creeps senden mit Geldkosten.
		-Geldeinkommen für Mobilespieler erhöht sich jede Runde.
		-Sieht die ganze lane aus Vogelperspektive.
	
	PC:
		-Ist eine Spielfigur in der Lane
		-Verteidigt die Basis
		-Wird durch Powerups stärker
		-First Person, Third Person oder Vogelperspektive

	Creeps:
		-3x Creep Spawnpoints
		-Spawn #1 und #3 erhöht
		-Creeps laufen auf ein Punkt zu. Wenn sie den Punkt erreicht haben, verliert das Team HP.
		-Powerups spawnen bei Creep spawns.
	
	