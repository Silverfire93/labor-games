﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class TeamManager : Photon.PunBehaviour
{

	#region Team variable

	public int hpRed;
	public int hpBlue;
	public int incRed;
	public int incBlue;
	public int moneyRed;
	public int moneyBlue;

	#endregion

	#region constants

	public static int START_HP = 50;
	public static int START_MONEY = 50;
	public static int START_INCOME = 50; 

	#endregion
	

	private float time = 999;
    private Animator anim;
    private bool gameOver;

	#region UI variables

	public Text txtHpRed;
	public Text txtHpBlue;
	public Text txtIncRed;
	public Text txtIncBlue;
	public Text txtMoneyRed;
	public Text txtMoneyBlue;
    public Text txtTimer;
    public Text txtGameOver;

    public Image fillImage;

	#endregion

	// Use this for initialization
	void Start ()
	{
        gameOver = false;
		resetToStartValues ();
    }

    void Awake()
    {
        anim = GameObject.Find("Canvas").transform.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update ()
	{	
		time -= Time.deltaTime;
		txtTimer.text = time.ToString("F1");
        fillImage.fillAmount = time / 20;

        if( !gameOver && hpRed <= 0)
        {
            gameOver = true;
            DisableAllPlayers();
            txtGameOver.text = "Team Blue Won!";
            anim.SetTrigger("GameOver");
        } else if (!gameOver && hpBlue <= 0)
        {
            gameOver = true;
            DisableAllPlayers();
            txtGameOver.text = "Team Red Won!";
            anim.SetTrigger("GameOver");
        }
	}

    private void DisableAllPlayers()
    {
        GameObject[] fpsPlayers = GameObject.FindGameObjectsWithTag("FPSPlayer");
        GameObject[] mobilePlayers = GameObject.FindGameObjectsWithTag("MobilePlayer");
        foreach (GameObject go in fpsPlayers)
        {
            go.GetComponent<FirstPersonController>().enabled = false;
        }
        foreach (GameObject go in mobilePlayers)
        {
            go.transform.Find("MobileUI").gameObject.SetActive(false);
        }
    }

	#region Setter

	[PunRPC]
	public void SetHpRed (int newVal)
	{
		hpRed = newVal;
		txtHpRed.text = hpRed.ToString ();
	}

	[PunRPC]
	public void SetHpBlue (int newVal)
	{
		hpBlue = newVal;
		txtHpBlue.text = hpBlue.ToString ();
	}

	[PunRPC]
	public void SetIncRed (int newVal)
	{
		incRed = newVal;
		txtIncRed.text = incRed.ToString ();
	}

	[PunRPC]
	public void SetIncBlue (int newVal)
	{
		incBlue = newVal;
		txtIncBlue.text = incBlue.ToString ();
	}

    [PunRPC]
    public void SetMoneyBlue(int newVal)
    {
        moneyBlue = newVal;
        txtMoneyBlue.text = moneyBlue.ToString();
    }

    [PunRPC]
    public void SetMoneyRed(int newVal)
    {
        moneyRed = newVal;
        txtMoneyRed.text = moneyRed.ToString();
    }

    [PunRPC]
	public void IncrHpRed (int newVal)
	{
		hpRed += newVal;
		txtHpRed.text = hpRed.ToString ();
	}

	[PunRPC]
	public void IncrHpBlue (int newVal)
	{
		hpBlue += newVal;
		txtHpBlue.text = hpBlue.ToString ();
	}

	[PunRPC]
	public void IncrIncRed (int newVal)
	{
		incRed += newVal;
		txtIncRed.text = incRed.ToString ();
	}

	[PunRPC]
	public void IncrIncBlue (int newVal)
	{
		incBlue += newVal;
		txtIncBlue.text = incBlue.ToString ();
	}

	[PunRPC]
	public void DecrHpRed (int newVal)
	{
		hpRed -= newVal;
		txtHpRed.text = hpRed.ToString ();
	}

	[PunRPC]
	public void DecrHpBlue (int newVal)
	{
		hpBlue -= newVal;
		txtHpBlue.text = hpBlue.ToString ();
	}

	[PunRPC]
	public void DecrIncRed (int newVal)
	{
		incRed -= newVal;
		txtIncRed.text = incRed.ToString ();
	}

	[PunRPC]
	public void DecrIncBlue (int newVal)
	{
		incBlue -= newVal;
		txtIncBlue.text = incBlue.ToString ();
	}

	[PunRPC]
	public void UpdateMoneyText ()
	{		
		txtMoneyRed.text = "$ " + moneyRed.ToString ();		 
		txtMoneyBlue.text = "$ " + moneyBlue.ToString ();
	}

	[PunRPC]
	public void AddIncToMoney ()
	{
		moneyRed += incRed;
		moneyBlue += incBlue;
	}

    [PunRPC]
    public void DecrMoneyRed(int decValue)
    {
        moneyRed -= decValue;
        txtMoneyRed.text = "$ " + moneyRed.ToString();
    }

    [PunRPC]
    public void DecrMoneyBlue(int decValue)
    {
        moneyBlue -= decValue;
        txtMoneyBlue.text = "$ " + moneyBlue.ToString();
    }

    public int GetMoneyRed()
    {
        return moneyRed;
    }

    public int GetMoneyBlue()
    {
        return moneyBlue;
    }

    [PunRPC]
	public void SyncTimer (float t) {
		time = t;
	}

	[PunRPC]
	public void resetToStartValues(){
		SetHpRed (START_HP);
		SetHpBlue (START_HP);
		SetIncRed (START_INCOME);
		SetIncBlue (START_INCOME);
		SetMoneyBlue(START_MONEY);
		SetMoneyRed(START_MONEY);
	}

	#endregion
}
