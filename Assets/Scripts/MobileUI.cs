﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class MobileUI : Photon.PunBehaviour
{
	private GameObject spawn1;
	private GameObject spawn2;
	private GameObject spawn3;

	public GameObject creep;

	/// <summary>
	/// Button Array that contains all Buttons we need to grey out sometimes
	/// 0 UpgradeDmg 
	/// 1 UpgradeHP
	/// 2 UpgradeSpeed
	/// 3 SendCreep
	/// </summary>
	public Button[] Buttons;
    public Text txtCost;
    public Text txtCreepCost;

    private GameObject myCreep;

	private int team;
	private int creepCost = 20; 
	private int creepValue = 2;
	private int upgradeCost = 10;
	private int spawnPos;
	private bool isWatchingRed;
	private TeamManager teamManager;
	private PhotonView photonView;


	private float hpUpgrade = 0.0f;
	private float speedUpgrade = 0.0f;
	private int dmgUpgrade = 0;

	private const int RED = 1;
	private const int BLUE = 2;

    // Use this for initialization
    void Start ()
	{
		teamManager = GameObject.Find ("TeamUI").transform.GetComponent<TeamManager> ();
		photonView = PhotonView.Get (teamManager.transform.GetComponent<PhotonView> ());
		team = (int)PhotonNetwork.player.CustomProperties ["team"];

		spawnPos = 2;
		string creepSpawnLocation = "";

		isWatchingRed = true;

		if (team == RED) {
			creepSpawnLocation = "Blue";
			SwitchCameraPos ();
		} else {
			creepSpawnLocation = "Red";
		}

		spawn1 = GameObject.FindGameObjectWithTag ("Spawn1" + creepSpawnLocation);
		spawn2 = GameObject.FindGameObjectWithTag ("Spawn2" + creepSpawnLocation);
		spawn3 = GameObject.FindGameObjectWithTag ("Spawn3" + creepSpawnLocation);
		//init destination for NavAgent
		creep.GetComponent<CreepMovement> ().destination = GameObject.FindGameObjectWithTag ("Target" + creepSpawnLocation).transform;
	}

	void Update ()
	{
		if (CheckMoneyForUpgrade ()) {
			Buttons [0].interactable = true;
			Buttons [1].interactable = true;
			Buttons [2].interactable = true;
		} else {
			//not enough money so buttons are greyed out
			Buttons [0].interactable = false;
			Buttons [1].interactable = false;
			Buttons [2].interactable = false;
		}

		if (CheckMoney ()) {

			Buttons [3].interactable = true;
		} else {
			//not enough money so buttons are greyed out
			Buttons [3].interactable = false;
		}
	}

	public void SpawnCreep ()
	{
		if (!CheckMoney ()) {
			return;
		}

        if(spawnPos == 1)
            myCreep = PhotonNetwork.Instantiate(creep.name, spawn1.transform.position, spawn1.transform.rotation, 0);
        else if(spawnPos > 1 && spawnPos < 5)
            myCreep = PhotonNetwork.Instantiate(creep.name, spawn2.transform.position, spawn2.transform.rotation, 0);
        else
            myCreep = PhotonNetwork.Instantiate(creep.name, spawn3.transform.position, spawn3.transform.rotation, 0);

		PhotonView pv = myCreep.GetComponent<PhotonView> ();	

		// upgrade spawned creep with stored values
		pv.RPC ("UpgradeMaxHealth", PhotonTargets.All, hpUpgrade);
		pv.RPC ("UpgradeHpDamage", PhotonTargets.All, dmgUpgrade);
		pv.RPC ("UpgradeSpeed", PhotonTargets.All, speedUpgrade);

		myCreep.GetComponent<NavMeshAgent> ().enabled = true;
		if (team == RED) {
			photonView.RPC ("DecrMoneyRed", PhotonTargets.All, creepCost);
			photonView.RPC ("IncrIncRed", PhotonTargets.All, creepValue);
		} else {
			photonView.RPC ("DecrMoneyBlue", PhotonTargets.All, creepCost);
			photonView.RPC ("IncrIncBlue", PhotonTargets.All, creepValue);
		}

		if (spawnPos < 5)
			spawnPos++;
		else
			spawnPos = 1;
	}

	public void payForUpgrade (int amount)
	{
		if (team == RED) {
			photonView.RPC ("DecrMoneyRed", PhotonTargets.All, amount);
		} else {
			photonView.RPC ("DecrMoneyBlue", PhotonTargets.All, amount);
		}
		//double cost for an upgrade each time the player buys an upgrade
		upgradeCost += upgradeCost;
        creepCost += 4;
        creepValue += 1;
        txtCost.text = "" + upgradeCost;
        txtCreepCost.text = "" + creepCost;
    }

	public void UpgradeHp ()
	{
		if (!CheckMoneyForUpgrade ()) {
			return;
		}
		hpUpgrade += 10;
		payForUpgrade (upgradeCost);
	}

	public void UpgradeSpeed ()
	{
		if (!CheckMoneyForUpgrade ()) {
			return;
		}
		speedUpgrade += 1;
		payForUpgrade (upgradeCost);
	}

	public void UpgradeDmg ()
	{
		if (!CheckMoneyForUpgrade ()) {
			return;
		}
		dmgUpgrade += 1;
		payForUpgrade (upgradeCost);
	}

	public bool CheckMoney ()
	{
		if (team == RED) {
			if (teamManager.GetMoneyRed () >= creepCost)
				return true;
		} else {
			if (teamManager.GetMoneyBlue () >= creepCost)
				return true;
		}
		return false;
	}

	public bool CheckMoneyForUpgrade ()
	{
		if (team == RED) {
			if (teamManager.GetMoneyRed () >= upgradeCost)
				return true;
		} else {
			if (teamManager.GetMoneyBlue () >= upgradeCost)
				return true;
		}
		return false;
	}

	public void SwitchCameraPos ()
	{
		Camera cam = transform.parent.GetComponentInChildren<Camera> ();

		if (isWatchingRed) {
			cam.transform.position = cam.transform.position + new Vector3 (-175, 0, 0);
			isWatchingRed = false;
		} else {
			cam.transform.position = cam.transform.position + new Vector3 (175, 0, 0);
			isWatchingRed = true;
		}
	}
}
