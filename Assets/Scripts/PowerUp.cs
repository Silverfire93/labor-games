﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PowerUp : Photon.PunBehaviour
{

	PhotonView pv;

	private static int DMG_POWERUP = 15;
	private static float FIRERATE_POWERUP = 0.075f;

	public const int PU_DMG = 1;
	public const int PU_FIRERATE = 2;

	private int powerUpType = PU_FIRERATE;

	public const int PU_RED = 1;
	public const int PU_BLUE = 2;

	private int team = 0;

	// Use this for initialization
	void Start ()
	{
        
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "FPSPlayer") {
			pv = col.gameObject.GetComponent<PhotonView> ();

			if (powerUpType == PU_DMG) {
                
				if (PhotonNetwork.isMasterClient) {
					Debug.Log ("DMG UP!");
					pv.RPC ("PowerUpFPSDamage", PhotonTargets.All, DMG_POWERUP);
				}
			} else {
                
				if (PhotonNetwork.isMasterClient) {
					Debug.Log ("Fire Rate UP!");
					pv.RPC ("PowerUpFPSFireRate", PhotonTargets.All, FIRERATE_POWERUP);
				}
			}
			GameObject.FindObjectOfType<FXManager> ().GetComponent<PhotonView> ().RPC ("PowerUpFx", PhotonTargets.All, this.transform.position);
			DestroyOtherPowerUp ();
			if (this.photonView.isMine)
				PhotonNetwork.Destroy (this.gameObject);
		}
	}

	// Destroys powerup of opposing team
	public void DestroyOtherPowerUp ()
	{
		GameObject[] powerUps = GameObject.FindGameObjectsWithTag ("PowerUp");
		foreach (GameObject go in powerUps) {
			if (team == PU_RED && go.GetComponent<PowerUp> ().GetTeam () == PU_BLUE) {
				Debug.Log ("DESTROYED BLUE POWERUP");
				if (go.GetComponent<PhotonView> ().isMine)
					PhotonNetwork.Destroy (go);
			} else if (team == PU_BLUE && go.GetComponent<PowerUp> ().GetTeam () == PU_RED) {
				Debug.Log ("DESTROYED RED POWERUP");
				if (go.GetComponent<PhotonView> ().isMine)
					PhotonNetwork.Destroy (go);
			}
		}
	}

	[PunRPC]
	public void SetPowerUpType (int pType)
	{
		powerUpType = pType;
	}

	[PunRPC]
	public void SetTeam (int pTeam)
	{
		team = pTeam;
	}

	public int GetTeam ()
	{
		return team;
	}
}
