using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Health. Ścript for every object that can be shot and die. 
/// </summary>
public class Health : MonoBehaviour {

	public float maxHp = 100f;
	float currentHp;


	// Use this for initialization
	void Start () {
		currentHp = maxHp;
	}
	/// <summary>
	/// RPC call that every NetworkClient knows that this object took the given amount of dmg.
	/// </summary>
	/// <param name="amount">Amount of dmg that is subtracted from health</param>
	[PunRPC]
	public void TakeDamage(float amount) {
		currentHp -= amount;

		if (currentHp <= 0) {
			Die ();
		}
	}

	[PunRPC]
	public void UpgradeMaxHealth(float amount){
		maxHp += amount;
	}

	/// <summary>
	/// Called when health reaches zero and object needs to be destroyed.
	/// </summary>
	void Die() {
		//only owner is allowed to destory the object
		if (GetComponent<PhotonView> ().isMine) {
			Debug.Log ("Die on a PhotonView " + this.name + " is called");
			PhotonNetwork.Destroy (gameObject);
		}
	}
}
