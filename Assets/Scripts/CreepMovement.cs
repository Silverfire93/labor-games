﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CreepMovement : MonoBehaviour
{

	[SerializeField]
	public Transform destination;

	public NavMeshAgent navMeshAgent;

	private GameObject teamManager;
	private PhotonView photonView;

	private int HP_DAMAGE = 1;

	// Use this for initialization
	void Start ()
	{
		navMeshAgent = this.GetComponent<NavMeshAgent> ();
		teamManager = GameObject.Find ("TeamUI");
		photonView = PhotonView.Get (teamManager.transform.GetComponent<PhotonView> ());
		if (navMeshAgent == null) {
			Debug.LogError ("NavMeshAgent Component is not attached to " + gameObject.name);
		} else {
			SetDestination ();
		}
	}

	private void SetDestination ()
	{
		if (destination != null) {
			Vector3 target = destination.transform.position;
			navMeshAgent.SetDestination (target);
		}
	}

	void OnTriggerEnter (Collider col)
	{ 
		if (col.gameObject.tag == "TargetRed") {
			Debug.Log ("COLLISION RED");
			photonView.RPC ("DecrHpRed", PhotonTargets.All, HP_DAMAGE);
			PhotonNetwork.Destroy (this.gameObject);
            
		} else if (col.gameObject.tag == "TargetBlue") {
			Debug.Log ("COLLISION BLUE");
			photonView.RPC ("DecrHpBlue", PhotonTargets.All, HP_DAMAGE);
			PhotonNetwork.Destroy (this.gameObject);
		}
	}

	[PunRPC]
	void UpgradeHpDamage (int amount)
	{
		HP_DAMAGE += amount;
	}

	[PunRPC]
	void UpgradeSpeed (float amount)
	{
		navMeshAgent.speed += amount;
		navMeshAgent.acceleration += amount / 2.0f;
	}
}
