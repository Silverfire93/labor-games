﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : Photon.PunBehaviour
{
	#region Constants

	public static int TEAM_RED = 1;
	public static int TEAM_BLUE = 2;

    public const int PU_RED = 1;
    public const int PU_BLUE = 2;

    public const int PU_DMG = 1;
    public const int PU_FIRERATE = 2;

    #endregion

    #region Public Properties

    static public GameManager Instance;

	#endregion

	#region Public Variables

	[Tooltip ("The prefab to use for representing the player")]
	public GameObject playerPrefab;

	[Tooltip ("The prefab to use for representing the player")]
	public GameObject mobilePlayerPrefab;

	[Tooltip ("Spawnlocation of the red player.")]
	public GameObject playerSpawnRed;

	[Tooltip ("Spawnlocation of the blue player.")]
	public GameObject playerSpawnBlue;

    public GameObject powerUpPrefab;

    public Text txtPu;

	#endregion

	private bool pcRed = false;
	private bool mobileRed = false;
    private bool puSwitch = true;
    private bool puSpawned = false;
    private Transform playerSpawn;
	private float incomeCooldown = 20.0f;
    private float puTextCooldown = 5.0f;
	private List<PhotonPlayer> playersRed = new List<PhotonPlayer> ();
	private List<PhotonPlayer> playersBlue = new List<PhotonPlayer> ();
	private PhotonView TMPhotonView;
    private const int PU_INTERVAL = 4;
    
    private int powerUpCounter;

	#region Public Methods


	public void LeaveRoom ()
	{
		PhotonNetwork.LeaveRoom ();
	}


	#endregion

	#region Private Methods

	void LoadArena ()
	{
		
		if (!PhotonNetwork.isMasterClient) {
			Debug.LogError ("PhotonNetwork : Trying to Load a level but we are not the master Client");
			return;
		}
		Debug.Log ("PhotonNetwork : Loading Level");
		PhotonNetwork.LoadLevel ("GameScene");
		// reset values to start values when arena is loaded
		TMPhotonView.RPC ("resetToStartValues",PhotonTargets.All);

	}


	#endregion

	#region MonoBehaviour CallBacks

	/// <summary>
	/// MonoBehaviour method called on GameObject by Unity during initialization phase.
	/// </summary>
	void Start ()
	{
		Instance = this;
        powerUpCounter = 1;
		TMPhotonView = PhotonView.Get(GameObject.Find("TeamUI").transform.GetComponent<PhotonView>());
		if (PhotonNetwork.isMasterClient) {
			TMPhotonView.RPC ("SyncTimer", PhotonTargets.All, incomeCooldown);
		}

		if ((int)PhotonNetwork.player.CustomProperties ["team"] == 0) { 					
			TeamAssignment ();
		}

		if ((int)PhotonNetwork.player.CustomProperties ["team"] == TEAM_RED) { 
			playerSpawn = playerSpawnRed.transform;
		} else {
			playerSpawn = playerSpawnBlue.transform;     
		}

		if (playerPrefab == null || mobilePlayerPrefab == null) {
			Debug.LogError ("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
		} else {
			Debug.Log ("We are Instantiating LocalPlayer from " + SceneManager.GetActiveScene ().name);
			// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
			//if (SystemInfo.deviceType == DeviceType.Handheld) {

			//Destory all Player objects for local players before spawning new ones
			PhotonNetwork.DestroyPlayerObjects (PhotonNetwork.player);
			if ((SystemInfo.deviceType == DeviceType.Desktop)) {			
				GameObject myPlayer = PhotonNetwork.Instantiate(this.playerPrefab.name, playerSpawn.position, Quaternion.identity, 0);
                ColorPlayer(myPlayer);
				myPlayer.GetComponent<FirstPersonController> ().enabled = true;
				myPlayer.transform.Find ("FirstPersonCharacter").gameObject.SetActive (true);
                myPlayer.transform.Find("FpsUI").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("Crosshair").gameObject.SetActive(true);
            } else {
				GameObject mobilePlayer = PhotonNetwork.Instantiate (this.mobilePlayerPrefab.name, new Vector3 (0f, 0f, 0f), Quaternion.identity, 0);
				mobilePlayer.transform.Find ("Camera").gameObject.SetActive (true);
				mobilePlayer.transform.Find ("MobileUI").gameObject.SetActive (true);
			}
		}
	}

	void Update ()
	{
		
		if (!PhotonNetwork.isMasterClient) {
			//This update should only run on the MasterClient
			return;
		}

		incomeCooldown -= Time.deltaTime;
        if (puSpawned)
        {
            puTextCooldown -= Time.deltaTime;
            if(puTextCooldown < 0)
            {
                this.photonView.RPC("TogglePUText", PhotonTargets.All, false);
                puSpawned = false;
            }
        }
		if (incomeCooldown < 0) {
			Debug.Log ("income for players");
			incomeCooldown = 20.0f;
			TMPhotonView.RPC ("AddIncToMoney", PhotonTargets.All);
			TMPhotonView.RPC ("UpdateMoneyText", PhotonTargets.All);
			TMPhotonView.RPC ("SyncTimer", PhotonTargets.All, incomeCooldown);

            powerUpCounter++;
            if(powerUpCounter == PU_INTERVAL)
            {
                SpawnPowerUp();
                Debug.Log("SPAWNED POWERUP");
            }
		}
	}


	#endregion

    public void ColorPlayer(GameObject pPlayer)
    {
        if((int) PhotonNetwork.player.CustomProperties["team"] == TEAM_RED)
        {
            pPlayer.transform.Find("Player").GetComponent<MeshRenderer>().material.color = Color.red;
        } else
        {
            pPlayer.transform.Find("Player").GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }

	#region Photon Messages


	public override void OnPhotonPlayerConnected (PhotonPlayer other)
	{
		Debug.Log ("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player 

		if (PhotonNetwork.isMasterClient) {
			Debug.Log ("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected
			TMPhotonView.RPC ("SyncTimer", PhotonTargets.All, incomeCooldown); // Sync Income cooldown to all players
			LoadArena ();
		}

		Debug.Log ("PLAYERLIST LENGTH: " + PhotonNetwork.playerList.Length);

		foreach (PhotonPlayer pl in PhotonNetwork.playerList) {
			Debug.Log ("PLAYER NAME: " + pl.NickName);
			Debug.Log ("PLAYER TYPE: " + pl.CustomProperties ["deviceType"]);
			Debug.Log ("TEAM TYPE: " + pl.CustomProperties ["team"]);
			Debug.Log ("----------------------");
		}
        powerUpCounter = 1;
    }

    public void SpawnPowerUp()
    {
        Transform puSpawn = DeterminePUSpawnLocation();

        GameObject puRed = PhotonNetwork.Instantiate(this.powerUpPrefab.name, puSpawn.position, Quaternion.identity, 0);
        GameObject puBlue = PhotonNetwork.Instantiate(this.powerUpPrefab.name, puSpawn.position + new Vector3(-175, 0, 0), Quaternion.identity, 0);

        PhotonView pvRed = puRed.GetComponent<PhotonView>();
        PhotonView pvBlue = puBlue.GetComponent<PhotonView>();

        pvRed.RPC("SetTeam", PhotonTargets.All, PU_RED);
        pvBlue.RPC("SetTeam", PhotonTargets.All, PU_BLUE);

        if (puSwitch) // Switch between DMG and Fire Rate PowerUp
        {
            pvRed.RPC("SetPowerUpType", PhotonTargets.All, PU_DMG);
            pvBlue.RPC("SetPowerUpType", PhotonTargets.All, PU_DMG);
            puSwitch = false;
        } else
        {
            pvRed.RPC("SetPowerUpType", PhotonTargets.All, PU_FIRERATE);
            pvBlue.RPC("SetPowerUpType", PhotonTargets.All, PU_FIRERATE);
            puSwitch = true;
        }
        puSpawned = true;
        puTextCooldown = 5.0f;
        this.photonView.RPC("TogglePUText", PhotonTargets.All, true);
        
        powerUpCounter = 1;
    }

    [PunRPC]
    public void TogglePUText(bool show)
    {
        txtPu.gameObject.SetActive(show);
    }

    public Transform DeterminePUSpawnLocation()
    {
        int i = UnityEngine.Random.Range(1, 4);
        return GameObject.FindGameObjectWithTag("PU" + i).transform;
    }

	private void AssignTeam (PhotonPlayer player)
	{
		if ((int)player.CustomProperties ["deviceType"] == (int)DeviceType.Desktop) { //Is PC player
			if (!pcRed) {
				player.SetCustomProperties (new ExitGames.Client.Photon.Hashtable () { { "team", TEAM_RED } });
				playersRed.Add (player);
			} else {
				player.SetCustomProperties (new ExitGames.Client.Photon.Hashtable () { { "team", TEAM_BLUE } });
				playersBlue.Add (player);
			}
		} else { //Is Mobile player
			if (!mobileRed) {
				player.SetCustomProperties (new ExitGames.Client.Photon.Hashtable () { { "team", TEAM_RED } });
				playersRed.Add (player);
			} else {
				player.SetCustomProperties (new ExitGames.Client.Photon.Hashtable () { { "team", TEAM_BLUE } });
				playersBlue.Add (player);
			}
		}
	}

	public void TeamAssignment ()
	{
		foreach (PhotonPlayer pl in PhotonNetwork.otherPlayers) {
			if ((int)pl.CustomProperties ["team"] == TEAM_RED &&
			    (int)pl.CustomProperties ["deviceType"] == (int)DeviceType.Desktop) { //Red Desktop Player exists
				pcRed = true;
			} else if ((int)pl.CustomProperties ["team"] == TEAM_RED &&
				(int)pl.CustomProperties ["deviceType"] == (int)DeviceType.Handheld) {//Red Mobile Player exists
				mobileRed = true;
			}
		}
		AssignTeam (PhotonNetwork.player);
	}

	public override void OnPhotonPlayerDisconnected (PhotonPlayer other)
	{
		Debug.Log ("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects


		if (PhotonNetwork.isMasterClient) {
			Debug.Log ("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected
			LoadArena ();
		}
	}

	/// <summary>
	/// Called when the local player left the room. We need to load the launcher scene.
	/// </summary>

	public override void OnLeftRoom ()
	{
		SceneManager.LoadScene (0);
	}

	#endregion
}