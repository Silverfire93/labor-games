﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour
{

	public AudioClip bulletFXAudio;
	public AudioClip powerUpAudio;
	//public AudioClip bulletImpactCreepFXAudio;
	public AudioClip[] bulletImpactWallFXAudios;

	public ParticleSystem bulletFireParticles;
	public ParticleSystem bulletHitParticles;
	public ParticleSystem bulletHitWallParticles;

	/// <summary>
	/// Responsible for playing Sounds and particle effects whenever a player shoots.
	/// </summary>
	/// <param name="startPos">Start position of the particle and Sound effect.</param>
	/// <param name="endPos">Hit point of the shot the player fired, or 50 units away from camera direction if nothing is hit.</param>
	/// <param name="rot">Rotation of the camera</param>
	/// <param name="hitWall">Flag if a wall or creep was hit</param>
	[PunRPC]
	void bulletFx (Vector3 startPos, Vector3 endPos, Quaternion rot, bool hitWall)
	{		
		//Debug.Log ("BulletFx");
		AudioSource.PlayClipAtPoint (bulletFXAudio, startPos, 0.1f);
		//Instantiate (bulletFireParticles, startPos, rot);
		if (hitWall) {			
			ParticleSystem ps = Instantiate (bulletHitWallParticles, endPos, Quaternion.identity);
			AudioSource.PlayClipAtPoint (bulletImpactWallFXAudios [UnityEngine.Random.Range (0, 6)], endPos, 5f);
			Destroy (ps.gameObject, 3);
		} else {			
			ParticleSystem ps = Instantiate (bulletHitParticles, endPos, Quaternion.identity);
			//AudioSource.PlayClipAtPoint (bulletImpactCreepFXAudio, endPos);
			Destroy (ps.gameObject, 3);
		}

	}

	[PunRPC]
	void PowerUpFx (Vector3 pos)
	{
		AudioSource.PlayClipAtPoint (powerUpAudio, pos, 0.5f);
	}





}
